package garages;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.NonNull;

import java.io.PrintStream;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Représente une voiture qui peut être stationnée dans des garages.
 */
@Getter
@ToString
@RequiredArgsConstructor // Génère un constructeur avec tous les arguments @NonNull ou final
public class Voiture {

	/**
	 * Le numéro d'immatriculation unique de la voiture.
	 */
	@NonNull
	private final String immatriculation;

	@ToString.Exclude // On ne veut pas afficher la liste des stationnements dans toString
	private final List<Stationnement> myStationnements = new LinkedList<>();

	/**
	 * Fait entrer la voiture dans le garage spécifié.
	 * Précondition : la voiture ne doit pas déjà être stationnée dans un garage.
	 *
	 * @param g le garage où la voiture va stationner
	 * @throws IllegalStateException si la voiture est déjà dans un garage
	 */
	public void entreAuGarage(Garage g) throws IllegalStateException {
		if (estDansUnGarage()) {
			throw new IllegalStateException("La voiture est déjà dans un garage");
		}
		// Pas dans un garage
		Stationnement s = new Stationnement(this, g);
		myStationnements.add(s);
	}

	/**
	 * Fait sortir la voiture de son garage actuel.
	 * Précondition : la voiture doit être stationnée dans un garage.
	 *
	 * @throws IllegalStateException si la voiture n'est pas actuellement dans un garage
	 */
	public void sortDuGarage() throws IllegalStateException {
		if (!myStationnements.isEmpty()) { // On a des stationnements
			// A partir de java 21, on pourrait utiliser la méthode getLast()
			int dernierGarage = myStationnements.size() - 1;
			Stationnement dernierStationnement = myStationnements.get(dernierGarage);
			if (dernierStationnement.estEnCours()) {
				dernierStationnement.terminer();
			} else {
				throw new IllegalStateException("La voiture est déjà sortie");
			}
		} else { // la liste est vide
			throw new IllegalStateException("La voiture n'a pas de stationnement");
		}
	}

	/**
	 * Retourne l'ensemble des garages visités par cette voiture.
	 *
	 * @return un Set d'objets Garage représentant tous les garages visités
	 */
	public Set<Garage> garagesVisites() {
		Set<Garage> result = new HashSet<>(); // Ensemble vide

		for (Stationnement s : myStationnements) // Pour chaque stationnement
		// On ajoute le garage au résultat
		{
			result.add(s.getGarageVisite());
		}
		return result;
		// ça peut s'écrire en une seule ligne !
		// return myStationnements.stream().map(s -> s.getGarageVisite()).collect(Collectors.toSet());
	}

	/**
	 * Vérifie si la voiture est actuellement stationnée dans un garage.
	 *
	 * @return vrai si la voiture est dans un garage, faux sinon
	 */
	public boolean estDansUnGarage() {

		boolean result = false;

		if (!myStationnements.isEmpty()) {
			// Avant java 21
//			Stationnement dernier = myStationnements.get(myStationnements.size() - 1);
			// A partir de java 21
			Stationnement dernier = myStationnements.getLast();

			result = dernier.estEnCours();
		}

		return result;
	}

	/**
	 * Pour chaque garage visité, imprime le nom de ce garage suivi de la liste des dates d'entrée / sortie dans ce
	 * garage
	 * <br>Exemple :
	 * <pre>
	 * Garage Castres:
	 *		Stationnement{ entree=28/01/2019, sortie=28/01/2019 }
	 *		Stationnement{ entree=28/01/2019, en cours }
	 *  Garage Albi:
	 *		Stationnement{ entree=28/01/2019, sortie=28/01/2019 }
	 * </pre>
	 *
	 * @param out l'endroit où imprimer (ex: System.out)
	 */
	public void imprimeStationnements(PrintStream out) {
		for (Garage garage : garagesVisites()) {
			out.printf("%s: %n", garage);
			for (Stationnement stationnement : myStationnements) {
				if (garage == stationnement.getGarageVisite()) {
					out.printf("\t%s %n", stationnement);
				}
			}
		}
	}

}
